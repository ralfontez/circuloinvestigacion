#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import with_statement
from PyQt5.QtWidgets import QDialog, QPushButton, QLabel, QWidget, QVBoxLayout, QTableWidget, QTableWidgetItem, QColorDialog
from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from ext.visualization import Visualization

class VisualizationUnivariateData(QWidget):

    def  __init__(self, dataset, index, parent=None):
        super(VisualizationUnivariateData, self).__init__(parent)
        self.dataset = dataset
        self.index = index
        self.x = None
        self.y = None
        self.color = 'b'
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/visualizationUnivariateDataForm.ui', self)

        headers = self.getcolHeaders()
        
        self.comboBoxVariable.addItems(headers)
        #self.cbAxisY.addItems(headers)

        self.comboBoxType.addItems(['line', 'hist', 'box', 'kde', 'area'])
        #self.cbStyle.addItems(['None','k.'])
        #self.btnColor.clicked.connect(self.openColorDialog)

        self.btnAccept.clicked.connect(self.plotDataXY)

    def plotDataXY(self):
        if self.index:
            xlabel = self.dataset.index.name
        
               
        visual = Visualization()
        self.mpl.canvas.ax.clear()
        
        visual.plot_univariate_data(dataset=self.dataset, x=None, y=self.comboBoxVariable.currentText(), ax=self.mpl.canvas.ax,
                        style=None, kind=self.comboBoxType.currentText(), 
                        xlabel=xlabel, ylabel=self.comboBoxVariable.currentText(), color=self.color)
        self.mpl.canvas.draw()

    #def getcolHeaders(self):
    #    if self.index:
    #        return self.dataset.reset_index().columns.values
    #    else:
    #        return self.dataset.columns.values
    
    def getcolHeaders(self):
        #if self.index:
        #    dataset = self.dataset.reset_index()
        
        column_names = self.dataset.columns.values
        column_with_missing_values = self.dataset.isnull().any()
        column_without_missing_values =  [not(e) for e in column_with_missing_values]
        return column_names[column_without_missing_values];

    def openColorDialog(self):
        color = QColorDialog.getColor()
        if color.isValid():
            self.color = color.name()
