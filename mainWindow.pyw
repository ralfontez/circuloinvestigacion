#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Main program
"""
import sys
from PyQt5.QtWidgets import (QMainWindow, QApplication, QMdiArea, QWidget, QAction,
                            QFileDialog, qApp, QDesktopWidget)
from PyQt5 import uic, QtGui, QtWidgets
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSignalMapper,
                        QSize, QTextStream, Qt)
from aboutDialog import *
from exploreData import WindowExplore
from setIndexColumn import IndexColumn
from cleanDataDialog import CleanData
from missingValuesDialog import MissingValues
from visualizationDataForm import VisualizationData
from visualizationUnivariateDataForm import VisualizationUnivariateData
from visualizationMultivariateDataForm import VisualizationMultivariateData
from descomposeSerieForm import DescomposeTimeSerie
from autocorrelationForm import Autocorrelation
from mlpForm import MlpMethod
from rnnForm import RnnMethod
import resources
import math

class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.mdiArea = QMdiArea()

        self.mdiArea.setViewMode(self.mdiArea.TabbedView)
        self.mdiArea.setTabsClosable(True)
        self.mdiArea.setTabsMovable(True)

        self.mdiArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.mdiArea.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.setCentralWidget(self.mdiArea)

        self.windowMapper = QSignalMapper(self)
        self.windowMapper.mapped[QWidget].connect(self.setActiveSubWindow)

        self.dataset = {}
        self.index = False

        #self.setWindowIcon(QIcon("resource/png/icon.png"))

        self.initUI()

    def initUI(self):
        uic.loadUi('ui/main.ui',self)
        self.setStyleSheet(open('resource/qss/style.qss', 'r').read())
        self.statusBar.showMessage('Bienvenido')

        self.menus()
        self.toolBars()


        

        flags = Qt.WindowFlags()

        
        flags = Qt.Window
        
        #flags |= Qt.MSWindowsFixedSizeDialogHint            
        #flags |= Qt.X11BypassWindowManagerHint
        #flags |= Qt.FramelessWindowHint
        flags |= Qt.WindowTitleHint
        flags |= Qt.WindowSystemMenuHint
        flags |= Qt.WindowMinimizeButtonHint
        flags |= Qt.WindowMaximizeButtonHint
        flags |= Qt.WindowCloseButtonHint
        flags |= Qt.WindowContextHelpButtonHint
        flags |= Qt.WindowShadeButtonHint
        #flags |= Qt.WindowStaysOnTopHint
        #flags |= Qt.WindowStaysOnBottomHint
        flags |= Qt.CustomizeWindowHint

        self.setWindowFlags(flags)

        screen = QtWidgets.QDesktopWidget().screenGeometry()

        self.setMaximumSize(screen.width(), screen.height())
        MIN_HEIGHT = 700
        phi = ((math.sqrt(5)+1)/2)
        MIN_WIDTH = int(MIN_HEIGHT*phi)
        self.setMinimumSize(MIN_WIDTH, MIN_HEIGHT)
        
        self.setGeometry(0,0, MIN_WIDTH, MIN_HEIGHT)

        self.center() 
        
        #self.showFullScreen()

    def menus(self):
        self.actionOpenFile.setShortcut('Ctrl+O')
        self.actionOpenFile.triggered.connect(self.openFile)
        self.actionExit.setShortcut('Ctrl+X')
        self.actionExit.triggered.connect(qApp.quit)

        self.actionAbout.triggered.connect(self.about)

        self.actionSetIndexColumn.triggered.connect(self.indexColumn)
        self.actionReplaceMissingValues.triggered.connect(self.replaceMissing)
        self.actionCleanData.triggered.connect(self.cleanData)
        
        self.actionVisualizeUnivariateData.triggered.connect(self.plotUnivariateDataset)
        self.actionVisualizeData.triggered.connect(self.plotDataset)        
        self.actionVisualizeMultivariateData.triggered.connect(self.plotMultivariateDataset)

        self.actionDescomposets.triggered.connect(self.descomposets)
        self.actionAcfPacf.triggered.connect(self.autocorrelationts)

        self.actionMLP.triggered.connect(self.mlpMethod)
        self.actionRNN.triggered.connect(self.rnnMethod)

    def toolBars(self):
        self.actionToolBarOpen.triggered.connect(self.openFile)
        self.actionToolBarRefresh.triggered.connect(self.refreshTable)
        self.actionToolBarSalir.triggered.connect(self.close)

        self.actionToolBarVisualizeData.triggered.connect(self.plotDataset)
        self.actionToolBarDecomposition.triggered.connect(self.descomposets)
        self.actionToolBarAutocorrelation.triggered.connect(self.autocorrelationts)

        self.actionToolBarSetIndexColumn.triggered.connect(self.indexColumn)
        self.actionToolBarReplaceMissingValues.triggered.connect(self.replaceMissing)
        self.actionToolBarCleanData.triggered.connect(self.cleanData)

        self.actionToolBarMLP.triggered.connect(self.mlpMethod)
        self.actionToolBarRNN.triggered.connect(self.rnnMethod)

    def openFile(self):
        self.mdiArea.closeAllSubWindows()
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"Abrir", "","Archivos de texto (*.csv *.txt);;Todos los archivos (*)", options=options)

        if fileName:
            self.statusBar.showMessage('Archivo: '+ fileName)
            self.explore = WindowExplore(fileName, parent=self)
            # self.setCentralWidget(self.explore)
            self.mdiArea.addSubWindow(self.explore)
            self.explore.show()
            self.dataset = self.explore.getDataset()
            self.indexColumn()

    def center(self):
        frameGm = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())

    def about(self):
        self.aboutDialog = AboutDialog()
        return self.aboutDialog.show()

    def indexColumn(self):
        if len(self.dataset)!=0:
            setIndexColumn = IndexColumn(self.dataset)
            setIndexColumn.show()
            if setIndexColumn.exec_():
                self.dataset = setIndexColumn.getDataset()
                self.index = True
                self.refreshTable()
        else:
            print("Dataset is empty")

    def refreshTable(self):
        if len(self.dataset)!=0:
            # dataset is not empty
            self.explore.updateTable(self.dataset)
        else:
            print("Dataset is empty")

    def cleanData(self):
        try:
            if len(self.dataset)!=0:
                cleanDialog = CleanData(dataset=self.dataset, index=self.index)
                cleanDialog.show()
                if cleanDialog.exec_():
                    self.dataset = cleanDialog.getDataset()
                # print(self.dataset)
            else:
                print("Dataset is empty")
        except Exception as e:
            print("type error: " + str(e))

    def replaceMissing(self):
        try:
            if len(self.dataset)!=0:
                missingValuesDialog = MissingValues(dataset=self.dataset, index=self.index)
                missingValuesDialog.show()
                if missingValuesDialog.exec_():                    
                    self.dataset = missingValuesDialog.getDataset()                
                
            else:
                print("Dataset is empty")
                
        except Exception as e:
            print("type error: " + str(e))

    def plotUnivariateDataset(self):
        try:
            if len(self.dataset)!=0:            
                plot = VisualizationUnivariateData(dataset=self.dataset, index=self.index)
                self.mdiArea.addSubWindow(plot)
                plot.show()
            else:
                print("Dataset is empty")
        except Exception as e:
            print("type error: " + str(e))

    def plotDataset(self):
        try:
            if len(self.dataset)!=0:            
                plot = VisualizationData(dataset=self.dataset, index=self.index)
                self.mdiArea.addSubWindow(plot)
                plot.show()
            else:
                print("Dataset is empty")
        except Exception as e:
            print("type error: " + str(e))

    def plotMultivariateDataset(self):
        try:
            if len(self.dataset)!=0:            
                plot = VisualizationMultivariateData(dataset=self.dataset, index=self.index)
                self.mdiArea.addSubWindow(plot)
                plot.show()
            else:
                print("Dataset is empty")
        except Exception as e:
            print("type error: " + str(e))

    def descomposets(self):
        if len(self.dataset)!=0:    
            descompose = DescomposeTimeSerie(dataset=self.dataset, index=self.index)
            self.mdiArea.addSubWindow(descompose)
            descompose.show()
        else:
            print("Dataset is empty")

    def autocorrelationts(self):
        if len(self.dataset)!=0:
            autocorr = Autocorrelation(dataset=self.dataset, index=self.index)
            self.mdiArea.addSubWindow(autocorr)
            autocorr.show()
        else:
            print("Dataset is empty")

    def mlpMethod(self):
        if len(self.dataset)!=0:
            mlp = MlpMethod(dataset=self.dataset, index=self.index)
            self.mdiArea.addSubWindow(mlp)
            mlp.show()
        else:
            print("Dataset is empty")

    def rnnMethod(self):
        if len(self.dataset)!=0:
            rnn = RnnMethod(dataset=self.dataset, index=self.index)
            self.mdiArea.addSubWindow(rnn)
            rnn.show()
        else:
            print("Dataset is empty")

    def setActiveSubWindow(self, window):
        if window:
            self.mdiArea.setActiveSubWindow(window)



if __name__=='__main__':
    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit(app.exec_())
