#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
This create an about dialog
"""

from PyQt5.QtWidgets import QDialog
from PyQt5 import uic
from PyQt5.QtGui import QPixmap
import resources

class AboutDialog(QDialog):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        uic.loadUi('ui/about.ui',self)

        laSalle = QPixmap(':logos/images/logo_lasalle.png')
        iiap = QPixmap(':logos/images/logo_iiap.png')
        concytec = QPixmap(':logos/images/logo_concytec.png')
        unsa = QPixmap(':logos/images/logo_unsa.png')
        pucp = QPixmap(':logos/images/logo_pucp.png')
        cienciactiva = QPixmap(':logos/images/logo_cienciactiva.png')

        self.lblSalle.setScaledContents(True)
        self.lblSalle.setPixmap(laSalle)
        self.lblIiap.setScaledContents(True)
        self.lblIiap.setPixmap(iiap)
        self.lblConcytec.setScaledContents(True)
        self.lblConcytec.setPixmap(concytec)
        self.lblUnsa.setScaledContents(True)
        self.lblUnsa.setPixmap(unsa)
        self.lblPucp.setScaledContents(True)
        self.lblPucp.setPixmap(pucp)
        self.lblCienciaActiva.setScaledContents(True)
        self.lblCienciaActiva.setPixmap(cienciactiva)

        self.btnAceptar.clicked.connect(self.close)
        self.btnAceptar.setShortcut('A')
