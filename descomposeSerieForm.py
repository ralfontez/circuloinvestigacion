#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import with_statement
from PyQt5.QtWidgets import QDialog, QPushButton, QLabel, QWidget, QVBoxLayout, QTableWidget, QTableWidgetItem, QColorDialog, QMessageBox
from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QIcon
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from ext.analysis import Analysis

class DescomposeTimeSerie(QWidget):

    def  __init__(self, dataset, index, parent=None):
        super(DescomposeTimeSerie, self).__init__(parent)
        self.dataset = dataset
        self.index = index
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/DescomposeSerieForm.ui', self)

        headers = self.getcolHeaders()
        self.comboBoxVariable.addItems(headers)
        self.comboBoxModel.addItems(['additive','multiplicative'])
        self.btnAccept.clicked.connect(self.plotDescompose)
        self.spinBoxFrequency.setMinimum(1)

    def plotDescompose(self):
        if self.index:
            
            #if self.spinBoxFrequency.value()>0:
            dictionary = self.dataset.min().to_dict()
            variable = self.comboBoxVariable.currentText()

            analysis = Analysis()  
            
            if self.comboBoxModel.currentText() =='multiplicative' and dictionary[variable]<=0:
                #msgBox = QMessageBox.Critical()

                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)

                msg.setText("Valor mininmo de '"+ variable +"' es "+ str(dictionary[variable]))
                msg.setInformativeText("Los valores de la variable deberia ser mayores que cero.")
                msg.setWindowTitle("ERROR")
                #msg.setDetailedText("Valor mininmo de '"+ variable +"' es "+ dictionary[variable])
                msg.setStandardButtons(QMessageBox.Ok)
                #msg.buttonClicked.connect(msgbtn)
                    
                retval = msg.exec_()
                if retval == QMessageBox.Ok:
                    return
                
                #reset_dataset = self.dataset.reset_index()
                #reset_dataset[variable] = reset_dataset[variable] - dictionary[variable]

                #print(reset_dataset.min().to_dict())
                #result = analysis.descompose_ts(dataset=reset_dataset, variable = variable,
                #                    freq=int(self.spinBoxFrequency.value()), model=self.comboBoxModel.currentText())
            else:
                result = analysis.descompose_ts(dataset=self.dataset, variable = variable,
                                    freq=int(self.spinBoxFrequency.value()), model=self.comboBoxModel.currentText())
            
            self.mpl.canvas.ax1.clear()
            self.mpl.canvas.ax1.plot(self.dataset[self.comboBoxVariable.currentText()], label='Original')
            self.mpl.canvas.ax1.legend(loc='best')
            self.mpl.canvas.ax2.clear()
            self.mpl.canvas.ax2.plot(result.trend, label='Trend')
            self.mpl.canvas.ax2.legend(loc='best')
            self.mpl.canvas.ax3.clear()
            self.mpl.canvas.ax3.plot(result.seasonal,label='Seasonality')
            self.mpl.canvas.ax3.legend(loc='best')
            self.mpl.canvas.ax4.clear()
            self.mpl.canvas.ax4.plot(result.resid, label='Residuals')
            self.mpl.canvas.ax4.legend(loc='best')
            self.mpl.canvas.draw()

        else:
            self.lbresult.setText('Debe establecer una columna Time como índice')


    #def getcolHeaders(self):
    #    if self.index:
    #        return self.dataset.reset_index().columns.values
    #    else:
    #        return self.dataset.columns.values

    def getcolHeaders(self):
        column_names = self.dataset.columns.values
        column_with_missing_values = self.dataset.isnull().any()
        column_without_missing_values =  [not(e) for e in column_with_missing_values]
        return column_names[column_without_missing_values];