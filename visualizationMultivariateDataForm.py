#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import with_statement
from PyQt5.QtWidgets import QDialog, QPushButton, QLabel, QWidget, QVBoxLayout, QTableWidget, QTableWidgetItem, QColorDialog
from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from ext.visualization import Visualization

class VisualizationMultivariateData(QWidget):

    def  __init__(self, dataset, index, parent=None):
        super(VisualizationMultivariateData, self).__init__(parent)
        self.dataset = dataset
        self.index = index
        self.x = None
        self.y = None
        self.color = 'b'
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/visualizationMultivariateDataForm.ui', self)

        column_headers = self.getcolHeaders()
        
        self.avail_model = QStandardItemModel()        
        for column_head in column_headers: 
            standardItem = QStandardItem(column_head)
            standardItem.setCheckable(True)
            self.avail_model.appendRow(standardItem)        
        self.listView.setModel(self.avail_model)

        #self.comboBoxVariable.addItems(headers)
        #self.cbAxisY.addItems(headers)

        self.comboBoxType.addItems(['line', 'box', 'kde'])
        #self.cbStyle.addItems(['None','k.'])
        #self.btnColor.clicked.connect(self.openColorDialog)

        self.btnAccept.clicked.connect(self.plotDataXY)

    def plotDataXY(self):
        if self.index:
            xlabel = self.dataset.index.name
        
        columns = []

        for row in range(self.avail_model.rowCount()):
            it = self.avail_model.item(row)
            if it.checkState() == Qt.Checked:
                columns.append(it.text())
        
        if len(columns)>0:
            visual = Visualization()
            self.mpl.canvas.ax.clear()
            
            visual.plot_multivariate_data(dataset=self.dataset, columns = columns, ax=self.mpl.canvas.ax,
                            style=None, kind=self.comboBoxType.currentText(), 
                            xlabel=xlabel, ylabel=None, color=self.color)
            self.mpl.canvas.draw()
        
            

    #def getcolHeaders(self):
    #    if self.index:
    #        return self.dataset.reset_index().columns.values
    #    else:
    #        return self.dataset.columns.values
    
    def getcolHeaders(self):
        #if self.index:
        #    dataset = self.dataset.reset_index()
        
        column_names = self.dataset.columns.values
        column_with_missing_values = self.dataset.isnull().any()
        column_without_missing_values =  [not(e) for e in column_with_missing_values]
        return column_names[column_without_missing_values];

    def openColorDialog(self):
        color = QColorDialog.getColor()
        if color.isValid():
            self.color = color.name()
