#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import with_statement
from PyQt5.QtWidgets import QPushButton, QSizePolicy, QLabel, QWidget, QVBoxLayout
from PyQt5 import uic
from PyQt5.QtCore import Qt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from ext.rnn import RNN
from ext.utils import Utils

class RnnCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        #self.ax1 = self.fig.add_subplot(311)
        self.ax2 = self.fig.add_subplot(311)
        self.ax3 = self.fig.add_subplot(312)
        self.fig.tight_layout()

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                QSizePolicy.Expanding,
                                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

class RnnMethod(QWidget):

    def  __init__(self, dataset, index, parent=None):
        super(RnnMethod, self).__init__(parent)
        self.dataset = dataset
        self.index = index
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/rnnForm.ui', self)

        self.canvas = RnnCanvas(self.rnn)
        self.rnn.layout = QVBoxLayout()
        self.rnn.layout.addWidget(self.canvas)
        self.rnn.setLayout(self.rnn.layout)

        headers = self.getcolHeaders()
        self.cbVariable.addItems(headers)
        self.cbNormalized.addItems(['False', 'True'])
        self.cbDebug.addItems(['False', 'True'])
        self.cbOptimizer.addItems(['adam','adagrad','adadelta','adamax','rmsprop'])
        self.cbKernelInitial.addItems(['random_normal', 'glorot_normal', 'lecun_normal'])
        self.cbActivation.addItems(['relu','linear','sigmoid'])
        self.btnRun.clicked.connect(self.runMethod)

    def runMethod(self):
        #self.canvas.ax1.clear()
        self.canvas.ax2.clear()
        self.canvas.ax3.clear()
        window = self.spLags.value()
        forecast_horizon = self.spHorizont.value()
        data = self.dataset[self.cbVariable.currentText()]
        util = Utils()
        train, test = util.split_train_test(data, prob=float(self.txtTrain.text())/100)
        x_train, y_train = util.sliding_window_rnn(train, window=window, forecast_horizon=forecast_horizon)
        x_test, y_test = util.sliding_window_rnn(test, window=window, forecast_horizon=forecast_horizon)

        hidden_layers = tuple(map(int, self.txtHiddenLayers.text().split(',')))
        activation = self.cbActivation.currentText()

        optimizer = self.cbOptimizer.currentText()
        kernel_initializer = self.cbKernelInitial.currentText()

        rnn_method = RNN(n_inputs=window, n_outputs=forecast_horizon)
        rnn_method(hidden_layers=hidden_layers, optimizer=optimizer,
                    activation=activation, kernel_initializer=kernel_initializer)

        history = rnn_method.train(x_train, y_train, epochs=self.spEpoch.value(), batch_size=self.spBatch.value(),
                                    verbose=1)

        #rnn_method.plot_history(ax=self.canvas.ax1)
        y_pred = rnn_method.predict(x_test)

        #print("y_pred=", y_pred)
        print("shapping *** ")
        print("y_pred.shape=", y_pred.shape)

        #print("y_pred=", y_pred)
        print("y_test.shape=", y_test.shape)

        util.plot_scatter_predictions(y_pred=y_pred, y_true=y_test, ax=self.canvas.ax2)
        util.plot_predictions(y_pred=y_pred, y_true=y_test, variable=self.cbVariable.currentText(), ax=self.canvas.ax3)
        self.canvas.draw()

        scores = rnn_method.evaluate(x_test, y_test)
        print(scores)
        print('rmse', np.sqrt(scores[0]))

    def getcolHeaders(self):
        if self.index:
            return self.dataset.reset_index().columns.values
        else:
            return self.dataset.columns.values
