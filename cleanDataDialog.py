#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QDialog, QPushButton, QLabel
from PyQt5 import uic
from ext.preprocessing import Preprocessing

class CleanData(QDialog):

    def __init__(self, dataset, index=None):
        super(CleanData, self).__init__()

        # self.setAttribute(Qt.WA_DeleteOnClose)
        self.dataset = dataset
        self.index = index
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/cleanData.ui',self)

        self.cbVar1.addItems(self.getcolHeaders())
        self.cbVar2.addItems(self.getcolHeaders())
        self.cbVar3.addItems(self.getcolHeaders())

        self.cbVar1.enable = True

        self.btnCancel.clicked.connect(self.reject)
        self.btnAccept.clicked.connect(self.replaceAndDeleteValues)
        self.btnAdd.clicked.connect(self.addOptions)

    def replaceAndDeleteValues(self):
        preprocess = Preprocessing()

        if self.index:
            if self.chbduplicateDelete.isChecked():
                self.dataset = preprocess.drop_duplicate(self.dataset, subset=self.index)
        else:
            print('Debe establecer una columna Time como Indice')# You should stablish one column as index

        self.accept()

    def getcolHeaders(self):
        return self.dataset.columns.values
        #return ["col1"]

    def addOptions(self):
        redb = QPushButton('Red', self)
        self.formLayout.addWidget(redb)

    def getDataset(self):
        return self.dataset
