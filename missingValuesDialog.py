#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QDialog, QPushButton, QLabel
from PyQt5 import uic
from ext.preprocessing import Preprocessing
from PyQt5.QtGui import *
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5 import QtCore

class MissingValues(QDialog):

    def __init__(self, dataset, index=None):
        super(MissingValues, self).__init__()

        self.dataset = dataset
        self.index = index
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/replaceMissingValues.ui',self)
                  

        self.cbMethod.addItems(['Interpolación lineal', 'Vecino más cercano', 'Interpolación spline orden cero', 'Interpolación spline lineal', 'Interpolación spline cuadrática', 'Interpolación spline cúbica','Media'])

        self.btnCancel.clicked.connect(self.reject)
        self.btnAccept.clicked.connect(self.replaceMissingValues)

        self.btnAdd.clicked.connect(self.addVariable)
        self.btnRemove.clicked.connect(self.removeVariable)
        
        self.btnAddAll.clicked.connect(self.addAllVariables)        
        self.btnRemoveAll.clicked.connect(self.removeAllVariables)
        #self.listvVariables.addItems([item for item in self.getcolHeaders()])

        # self.listViewVariables.clicked.connect(self.listclicked)
        
        self.avail_model = QStandardItemModel()        
        for column in self.getcolHeaders(): 
            standardItem = QStandardItem(column)
            standardItem.setCheckable(True)
            self.avail_model.appendRow(standardItem)        
        self.listViewVariables.setModel(self.avail_model)

        self.sel_model = QStandardItemModel()
        self.listViewSelectedVariables.setModel(self.sel_model)
        
        # for item in self.getcolHeaders():
        #     Qitem = QListWidgetItem(item)
        #     self.listvVariables.addItem(Qitem)

    def listclicked(self, index):
        print("selected index", index)

    def replaceMissingValues(self):
        #preprocess = Preprocessing()

        

        #columns = self.dataset.isnull().any()

        
        
        #for row in range(self.sel_model.rowCount()):
        #    it = self.sel_model.item(row)
        #    print(it.text())
        #    values[it.text()] = 0

        if self.cbMethod.currentIndex()==0:
            print('Linear interpolation .. ')

            index_name = self.dataset.index.name
            print("name of the index: ", index_name)

            reset_dataset = self.dataset.reset_index()
            dataset = reset_dataset.interpolate(method='linear', axis=0)

            columns = []

            for row in range(self.sel_model.rowCount()):
                standardItem = self.sel_model.item(row)
                # print(it.text())
                column_name = standardItem.text()
                #print("Column name: ", column_name)
                columns.append(column_name)
                #reset_dataset[column_name] = dataset[column_name]
                #values[it.text()] = 0
            
            print("columns: ", columns)
            reset_dataset[columns] = dataset[columns]
            self.dataset = reset_dataset.set_index(index_name)
            
        elif self.cbMethod.currentIndex()==1: # 'linear', ‘nearest’, ‘zero’, ‘slinear’, ‘quadratic’ and ‘cubic’,
            print('Nearest neigbour interpolation .. ')

            index_name = self.dataset.index.name
            #print("name of the index: ", index_name)

            reset_dataset = self.dataset.reset_index()
            dataset = reset_dataset.interpolate(method='nearest', axis=0)            

            columns = []

            for row in range(self.sel_model.rowCount()):
                standardItem = self.sel_model.item(row)
                # print(it.text())
                column_name = standardItem.text()
                #print("Column name: ", column_name)
                columns.append(column_name)
                #reset_dataset[column_name] = dataset[column_name]
                #values[it.text()] = 0
            #print("columns: ", columns)
            reset_dataset[columns] = dataset[columns]
            self.dataset = reset_dataset.set_index(index_name)
             
        elif self.cbMethod.currentIndex()==2: 
            print('Spline interpolation of zeroth .. ')

            index_name = self.dataset.index.name
            #print("name of the index: ", index_name)

            reset_dataset = self.dataset.reset_index()
            dataset = reset_dataset.interpolate(method='zero', axis=0)            

            columns = []

            for row in range(self.sel_model.rowCount()):
                standardItem = self.sel_model.item(row)
                # print(it.text())
                column_name = standardItem.text()
                #print("Column name: ", column_name)
                columns.append(column_name)
                #reset_dataset[column_name] = dataset[column_name]
                #values[it.text()] = 0
            #print("columns: ", columns)
            reset_dataset[columns] = dataset[columns]
            self.dataset = reset_dataset.set_index(index_name)
        
        elif self.cbMethod.currentIndex()==3: 
            print('Spline interpolation of first .. ')

            index_name = self.dataset.index.name
            #print("name of the index: ", index_name)

            reset_dataset = self.dataset.reset_index()
            dataset = reset_dataset.interpolate(method='slinear', axis=0)            

            columns = []

            for row in range(self.sel_model.rowCount()):
                standardItem = self.sel_model.item(row)
                # print(it.text())
                column_name = standardItem.text()
                #print("Column name: ", column_name)
                columns.append(column_name)
                #reset_dataset[column_name] = dataset[column_name]
                #values[it.text()] = 0
            #print("columns: ", columns)
            reset_dataset[columns] = dataset[columns]
            self.dataset = reset_dataset.set_index(index_name)

        elif self.cbMethod.currentIndex()==4: 
            print('Spline interpolation of second .. ')

            index_name = self.dataset.index.name
            #print("name of the index: ", index_name)

            reset_dataset = self.dataset.reset_index()
            dataset = reset_dataset.interpolate(method='quadratic', axis=0)            

            columns = []

            for row in range(self.sel_model.rowCount()):
                standardItem = self.sel_model.item(row)
                # print(it.text())
                column_name = standardItem.text()
                #print("Column name: ", column_name)
                columns.append(column_name)
                #reset_dataset[column_name] = dataset[column_name]
                #values[it.text()] = 0
            #print("columns: ", columns)
            reset_dataset[columns] = dataset[columns]
            self.dataset = reset_dataset.set_index(index_name)
        
        elif self.cbMethod.currentIndex()==5: 
            print('Spline interpolation of third .. ')

            index_name = self.dataset.index.name
            #print("name of the index: ", index_name)

            reset_dataset = self.dataset.reset_index()
            dataset = reset_dataset.interpolate(method='cubic', axis=0)            

            columns = []

            for row in range(self.sel_model.rowCount()):
                standardItem = self.sel_model.item(row)
                # print(it.text())
                column_name = standardItem.text()
                #print("Column name: ", column_name)
                columns.append(column_name)
                #reset_dataset[column_name] = dataset[column_name]
                #values[it.text()] = 0
            #print("columns: ", columns)
            reset_dataset[columns] = dataset[columns]
            self.dataset = reset_dataset.set_index(index_name)
        
        else:
            print("Replacing with the average .. ")
            values = self.dataset.mean().to_dict()
            self.dataset = self.dataset.fillna(value=values)
            #it = self.sel_model.takeRow(row)
            #self.avail_model.insertRow(0, it)
            
        #print(self.cbMethod.count())
        #print(self.cbMethod.currentIndex())
        #print(self.cbMethod.currentText())

        self.accept()

    def addVariable(self):
        
        for row in reversed(range(self.avail_model.rowCount())):
            it = self.avail_model.item(row)
            if it.checkState() == QtCore.Qt.Checked:
                it = self.avail_model.takeRow(row)
                self.sel_model.insertRow(0, it)
        

        #print("rowCount : ", self.listViewVariables.model().rowCount())
        #print("rowCount selected: ", self.listViewSelectedVariables.model().rowCount())
                
        #it = self.listViewVariables.model().takeRow(0)
        #self.listViewVariables.model().removeRow(0)
        #self.listViewSelectedVariables.model().insertRow(0, it)
        #self.listViewSelectedVariables.model().appendRow(it)

        #self.listViewVariables.model().setItem(0, QStandardItem("hola"))
        
        #self.listViewSelectedVariables.model().appendRow(QStandardItem("hola"))
        #self.listViewVariables.model().clear()
        #print('add variable')
    
    def removeVariable(self):
        
        for row in reversed(range(self.sel_model.rowCount())):
            it = self.sel_model.item(row)
            if it.checkState() == QtCore.Qt.Checked:
                it = self.sel_model.takeRow(row)
                self.avail_model.insertRow(0, it)

        #print("rowCount : ", self.listViewVariables.model().rowCount())
        #print("rowCount selected: ", self.listViewSelectedVariables.model().rowCount())

        #print('remove variable')
    
    def addAllVariables(self):
        
        for row in reversed(range(self.avail_model.rowCount())):
            it = self.avail_model.takeRow(row)
            self.sel_model.insertRow(0, it)

        #print('add all variables')

    def removeAllVariables(self):
        for row in reversed(range(self.sel_model.rowCount())):
            it = self.sel_model.takeRow(row)
            self.avail_model.insertRow(0, it)

        #print('remove all variables')

    def getDataset(self):
        return self.dataset

    def getcolHeaders(self):
        column_names = self.dataset.columns.values
        column_with_missing_values = self.dataset.isnull().any()
        return column_names[column_with_missing_values];

        #if self.index:
        #    return self.dataset.columns.values
        #    #return self.dataset.reset_index().columns.values
        #else:
        #    return self.dataset.columns.values
