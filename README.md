## Installation

This application requires [PyQT5](http://pyqt.sourceforge.net/Docs/PyQt5/) to run.


## Installation of PyQt5

 1  Make sure that you have installed Python 3.6.5 (https://www.python.org/)

 2  Open terminal in that directory and perform the following commands:
```sh
	apt install python3-pip
	apt-get install build-essential
```
 3  Now you can install packages with Python 3.6 using pip3 command
```sh
	pip3 install pyqt5
    pip3 install numpy
    pip3 install pandas
    pip3 install sklearn
    pip3 install scipy
    python3 -mpip install matplotlib
    pip3 install --upgrade --no-deps statsmodels
    pip3 install patsy
    pip3 install keras
    pip3 install tensorflow
	
	apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
	apt-get install python3-tk
```

### Useful links

 - [PyQt5](http://pyqt.sourceforge.net/Docs/PyQt5/installation.html)


**Yeah!** 
