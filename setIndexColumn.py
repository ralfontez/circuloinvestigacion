#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pandas as pd
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QComboBox, QDialog, QDialogButtonBox,
                        QFormLayout, QGroupBox, QLabel, QLineEdit,
                        QPushButton, QVBoxLayout)
from datetime import datetime


class IndexColumn(QDialog):

    def __init__(self, dataset):
        super(IndexColumn, self).__init__()

        # self.setAttribute(Qt.WA_DeleteOnClose)
        self.dataset = dataset
        self.initUI()

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.setIndexColumnFormat)
        buttonBox.rejected.connect(self.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(buttonBox)
        self.setLayout(mainLayout)
        self.setWindowTitle('Establecer columna índice')

    def initUI(self):
        self.formGroupBox = QGroupBox('Seleccione la columna índice y el formato')
        layout = QFormLayout()

        column_names = self.dataset.columns.values
        column_with_missing_values = self.dataset.isnull().any()
        column_without_missing_values =  [not(e) for e in column_with_missing_values]
        

        self.cbColumn = QComboBox()
        self.cbColumn.addItems(column_names[column_without_missing_values])
        self.textEditor = QLineEdit()
        self.textEditor.setPlaceholderText('%Y-%m-%d')
        self.txtformat = QLabel('Formato: ')
        layout.addRow(QLabel('Columna Tiempo'), self.cbColumn)
        layout.addRow(self.txtformat, self.textEditor)
        self.formGroupBox.setLayout(layout)

    def setIndexColumnFormat(self):
        subset = self.cbColumn.currentText()
        print("Establishing column index as:", subset)
        dfColumnSubset = pd.to_datetime(self.dataset[subset], format=self.textEditor.text())
        #dfColumnSubset = pd.to_datetime(self.dataset[subset], format=self.txtformat.text())
        self.dataset.drop(labels=[subset], axis=1, inplace=True)
        self.dataset.insert(0, subset, dfColumnSubset)
        self.dataset = self.dataset.set_index(subset)
        self.accept()

    def getDataset(self):
        return self.dataset

    def getIndexColumn(self):
        return self.cbColumn.currentText()
