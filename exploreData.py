#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
This create an window explore data
"""

from PyQt5.QtWidgets import QDialog, QPushButton, QLabel, QWidget, QVBoxLayout, QTableWidget, QTableWidgetItem
from PyQt5 import uic
from PyQt5.QtCore import Qt
from ext.utils import Utils

class WindowExplore(QWidget):

    def  __init__(self, fileName, parent=None):
        super(WindowExplore, self).__init__(parent)

        self.setAttribute(Qt.WA_DeleteOnClose)
        self.isUntitled = True
        self.fileName = fileName
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/exploreData.ui',self)

        self.layout = QVBoxLayout()
        self.createTable(self.fileName)
        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)

    def createTable(self, fileName):
        util = Utils()
        self.dataset = util.load_data(filename=fileName)
        self.rowHeaders = self.dataset.index
        self.colHeaders = self.dataset.columns.values

        self.rowCount = len(self.rowHeaders)
        self.colCount = len(self.colHeaders)

        tableWidget = QTableWidget()
        tableWidget.setRowCount(self.rowCount)
        tableWidget.setColumnCount(self.colCount)
        tableWidget.setHorizontalHeaderLabels(self.colHeaders)

        for row  in range(self.rowCount):
            for column in range(self.colCount):
                newItem = QTableWidgetItem(str(self.dataset.iat[row,column]))
                newItem.setFlags(Qt.ItemIsEnabled)  # Read only
                tableWidget.setItem(row, column, newItem)

        self.tableWidget = tableWidget

    def getcolHeaders(self):
        return self.colHeaders

    def getDataset(self):
        return self.dataset

    def updateTable(self, dataset):
        self.dataset = dataset.reset_index()  # for visualization index
        self.rowHeaders = self.dataset.index
        self.colHeaders = self.dataset.columns.values

        self.rowCount = len(self.rowHeaders)
        self.colCount = len(self.colHeaders)

        tableWidget = QTableWidget()
        tableWidget.setRowCount(self.rowCount)
        tableWidget.setColumnCount(self.colCount)
        tableWidget.setHorizontalHeaderLabels(self.colHeaders)

        for row  in range(self.rowCount):
            for column in range(self.colCount):
                newItem = QTableWidgetItem(str(self.dataset.iat[row,column]))
                newItem.setFlags(Qt.ItemIsEnabled)  # Read only
                tableWidget.setItem(row, column, newItem)

        self.layout.removeWidget(self.tableWidget)
        self.tableWidget = tableWidget
        self.layout.addWidget(self.tableWidget)

    def closeEvent(self, event):
        event.accept()
