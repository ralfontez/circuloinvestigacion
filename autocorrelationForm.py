#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import with_statement
from PyQt5.QtWidgets import QPushButton, QSizePolicy, QLabel, QWidget, QVBoxLayout
from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from ext.analysis import Analysis

class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.ax1 = self.fig.add_subplot(211)
        self.ax2 = self.fig.add_subplot(212)

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                QSizePolicy.Expanding,
                                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

class Autocorrelation(QWidget):

    def  __init__(self, dataset, index, parent=None):
        super(Autocorrelation, self).__init__(parent)
        self.dataset = dataset
        self.index = index
        self.initUI()

    def initUI(self):
        uic.loadUi('ui/autocorrelationForm.ui', self)

        self.canvas = MplCanvas(self.mplotlib, width=10, height=8)
        self.mplotlib.layout = QVBoxLayout()
        self.mplotlib.layout.addWidget(self.canvas)
        self.mplotlib.setLayout(self.mplotlib.layout)

        headers = self.getcolHeaders()
        self.cbVariable.addItems(headers)

        self.spLags.maximum = 10
        self.spLags.minimum = 5
        
        self.btnAccept.clicked.connect(self.plotAutocorrelations)

    def plotAutocorrelations(self):
        analysis = Analysis()

        self.canvas.ax1.clear()
        self.canvas.ax2.clear()

        if self.chbAcf.isChecked():
            if self.spLags.value()>0:
                acf = analysis.acf(self.dataset[self.cbVariable.currentText()], ax=self.canvas.ax1, max_lags=self.spLags.value())
            else:
                acf = analysis.acf(self.dataset[self.cbVariable.currentText()], ax=self.canvas.ax1, max_lags=None)

        if self.chbPacf.isChecked():
            if self.spLags.value()>0:
                pcf = analysis.pacf(self.dataset[self.cbVariable.currentText()], ax=self.canvas.ax2, max_lags=self.spLags.value())
            else:
                pcf = analysis.pacf(self.dataset[self.cbVariable.currentText()], ax=self.canvas.ax2, max_lags=None)

        self.canvas.draw()

    #def getcolHeaders(self):
    #    if self.index:
    #        return self.dataset.reset_index().columns.values
    #    else:
    #        return self.dataset.columns.values

    def getcolHeaders(self):
        column_names = self.dataset.columns.values
        column_with_missing_values = self.dataset.isnull().any()
        column_without_missing_values =  [not(e) for e in column_with_missing_values]
        return column_names[column_without_missing_values];