import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import MinMaxScaler

class Utils:

    def load_data(self, filename, parse_dates=False, usecols=None, index_col=None, header=0, sep=','):
        dataset = pd.read_csv(filename, parse_dates=parse_dates, index_col=index_col, sep=sep, usecols=usecols,
                              header=header, infer_datetime_format=True)
        return dataset


    def split_train_test(self, dataset, prob=0.8):
        print('split_train_test++++')
        print('dataset.shape: ', dataset.shape)
        train, test = np.split(dataset, [int(prob*len(dataset))])
        return train, test

    #def sliding_window(self, dataset, window=1, forecast_horizon=1):
    #    dataX, dataY = [], []
    #    for i in range(dataset.shape[0] - window - forecast_horizon + 1):
    #        dataX.append(dataset[i:(i + window)])
    #        dataY.append(dataset[(i + window) : (i + window + forecast_horizon)])
    #
    #    return np.array(dataX), np.array(dataY)
    
    def sliding_window(self, dataset, window=1, forecast_horizon=1):
        dataX, dataY = [], []
        scaler = MinMaxScaler(feature_range=(0,1))
        for i in range(dataset.shape[0] - window - forecast_horizon + 1):
            dataX.append(dataset[i:(i + window)].values)
            dataY.append(dataset[(i + window) : (i + window + forecast_horizon)].values)
        '''dataX, dataY = scaler.fit_transform(np.array(dataX)), scaler.fit_transform(np.array(dataY))
        return dataX, dataY'''
        return np.array(dataX), np.array(dataY)

    def sliding_window_rnn(self, dataset, window=1, forecast_horizon=1):
        scaler = MinMaxScaler(feature_range=(0,1))
        dataX, dataY = [], []
        for i in range(dataset.shape[0] - window - forecast_horizon + 1):
            dataX.append(dataset[i:(i + window)].values)
            dataY.append(dataset[(i + window) : (i + window + forecast_horizon)].values)
        print("np.array(dataX): ", np.array(dataX))
        print("np.array(dataY): ", np.array(dataY))
        dataX, dataY = scaler.fit_transform(np.array(dataX)), scaler.fit_transform(np.array(dataY))
        #dataX, dataY = np.array(dataX), np.array(dataY)
        return np.reshape(dataX, (dataX.shape[0], dataX.shape[1], 1)), dataY


    def series_to_supervised(self, dataset, window=1, forecast_horizon=1):
        n_vars = len(list(dataset.columns.values))
        cols, names = list(), list()
        for i in range(window, 0, -1):
            cols.append(dataset.shift(i))
            names += [('var%d(t-%d)' % (j+1,i)) for j in range(n_vars)]

        for i in range(0, forecast_horizon):
            cols.append(dataset.shift(-i))
            if i == 0:
                names += [('var%d(t)' %(j+1)) for j in range(n_vars)]
            else:
                names += [('var%d(t+%d)' % (j+1,i)) for j in range(n_vars)]

        dataset_xy = pd.concat(cols, axis=1)
        dataset_xy.columns = names
        dataset_xy.dropna(inplace=True)

        return dataset_xy

    def plot_scatter_predictions(self, y_pred, y_true, ax):
        #y_pred = y_pred.reshape(y_pred.shape[0],)
        #y_true = y_true.reshape(y_true.shape[0],)
        y_pred = y_pred.reshape(y_pred.shape[0]*y_pred.shape[1],)
        y_true = y_true.reshape(y_true.shape[0]*y_true.shape[1],)
        m, b = np.polyfit(y_true, y_pred, 1)
        #r2 = r2_score(y_pred, y_true)
        r2 = r2_score(y_true, y_pred)
        #ax.scatter(y_pred, y_true, label='Predictions', edgecolor='black')
        ax.scatter(y_true, y_pred, label='Predictions', edgecolor='black')
        ax.plot(y_true, m * y_true + b, 'r-')
        props = dict(boxstyle='square', facecolor='white', alpha=0.5)
        text = '$r^2$ = ' + str(round(r2, 3))
        ax.text(0.5, 0.5, text, fontsize=12, verticalalignment='center', bbox=props)
        #ax.set_xlabel('Predicted')
        #ax.set_ylabel('Observed')

        ax.set_xlabel('Observed')
        ax.set_ylabel('Predicted')
        # ax.legend(loc='upper left')

    def plot_predictions(self, y_pred, y_true, variable, ax):
        if y_true.shape[1]==1:
            ax.plot(y_true, label='Test', color='b')
            ax.plot(y_pred, label='Prediction', color='g')
            ax.set_xlabel('Time')
            ax.set_ylabel(variable)
            ax.legend()
        elif y_true.shape[1]>1:
            y_true = np.array(y_true)[:,0]        
            y_pred = np.array(y_pred)[:,0]
            ax.plot(y_true, label='Test', color='b')
            ax.plot(y_pred, label='Prediction', color='g')
            ax.set_xlabel('Time')
            ax.set_ylabel(variable)
            ax.legend()
        else:
            print("ERROR: in utils.py")
        
