# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf
import statsmodels.api as sm

class Analysis:
    """ Class for defining time series analysis """

    def descompose_ts(self, dataset, variable, freq=1, model='additive', filt=None, two_sided=True):
        descomposition = seasonal_decompose(x=dataset[variable], model=model, freq=freq, two_sided=two_sided)
        return descomposition

    def stationarity_mean_std(self, dataset, window):
        mean = dataset.rolling(window=window).mean()
        std = dataset.rolling(window=window).std()
        fig = plt.figure(figsize=(12,8))
        plt.plot(dataset, color='blue', label='Serie')
        plt.plot(mean, color='red', label='Media')
        plt.plot(std, color='green', label='Desviación estándar')
        plt.legend(loc='best')
        plt.title('Media  & desviación estándar, ventana:' + str(window))
        plt.show()

    def test_dicker_fuller(self, dataset, variable):
        ts = dataset[variable]
        test = adfuller(ts, autolag='AIC')
        result = pd.Series(test[0:4], index=['Test estadístico','p-valor','#Regazos usados','Número de observaciones usadas'])
        for key, value in test[4].items():
            result['Valor crítico (%s)' %key] = value
        return result

    def correlation_lags(self, dataset, lags):
        values = pd.DataFrame(dataset.values)
        columns = [values]
        for i in range(1, (lags+1)):
            columns.append(values.shift(i))
        df = pd.concat(columns, axis=1)

        columns = ['t']
        for i in range(1, (lags+1)):
            columns.append('lag ' + str(i))

        df.columns = columns
        result = df.corr()
        return result

    def acf(self, dataset, ax, max_lags=None):
        plot_acf(dataset, ax=ax, lags=max_lags)

        #if max_lags is None:
        #    lags = np.arange(len(dataset))
        #    max_lags = len(lags) - 1

        #values = sm.tsa.stattools.acf(dataset, nlags=max_lags)
        return None

    def pacf(self, dataset, ax, max_lags=None):
        plot_pacf(dataset, ax=ax, lags=max_lags)

        if max_lags is None:
            lags = np.arange(len(dataset))
            max_lags = len(lags) - 1

        values = sm.tsa.stattools.pacf(dataset, nlags=max_lags)
        return values
