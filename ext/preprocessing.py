import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

class Preprocessing:
    """ Class for defining preprocessing """

    def drop_duplicate(self, dataset, subset=None):
        return dataset.reset_index().drop_duplicates(subset=subset, keep='last').set_index(subset)

    def fillmissing(self, dataset, value, subset):
        dataset = dataset.set_index[subset]
        features = list(dataset.columns.values)

        for feature in features:
            index = dataset[feature] == value
            dataset[index, feature] = np.nan

        return dataset

    def interpolation(self, dataset, method='linear'):
        return dataset.interpolate(method=method)

    def normalization(self, dataset, feature_range=(0,1)):
        values = dataset.values
        scaler = MinMaxScaler(feature_range=feature_range)
        scaler = scaler.fit(values)
        normalized = scaler.transform(values)

        return scaler, normalized

    def inverse_normalization(self, values, scaler):
        return scaler.inverse_transform(values)

    def standardization(self, dataset):
        values = dataset.values
        scaler = StandardScaler()
        scaler = scaler.fit(values)
        standardized = scaler.transform(values)

        return scaler, standardized

    def inverse_standardization(self, values, scaler):
        return scaler.inverse_transform(values)
