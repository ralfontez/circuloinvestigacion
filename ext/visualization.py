# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import lag_plot
from pandas.plotting import autocorrelation_plot
#from pandas.plotting import autocorrelation_plot

class Visualization:
    """ Class for defining time series visualization """

    def plot_univariate_data(self, dataset, ax, x=None, y=None, kind='line', title=None, style=None, xlabel='', ylabel='', color='b'):
    
        #self.comboBoxType.addItems(['line','bar', 'hist', 'box', 'kde', 'area', 'pie'])

        if kind == 'line':# line
            dataset.plot(x=None, y=y, ax=ax, kind='line', title='line', subplots=True, style=None, legend=True, color='b')
            #dataset.plot(x=None, y='length', ax=ax, kind='line', title='title1', subplots=True, style=None, legend=True, color='r')
        elif kind =='bar':  # bar
            dataset.plot(x=None, y=y, ax=ax, kind='bar', title='bar', subplots=True, legend=True, xticks=None, color='b')
        elif kind == 'hist':  # hist
            dataset.plot(x=None, y=y, ax=ax, kind='hist', title='hist', subplots=True, legend=True, color='b')
        elif kind=='box': # box
            #ax = df.boxplot(column=['width', 'length'])
            dataset.plot(x=None, y=y, ax=ax, kind='box', title='box plot', grid=True, subplots=True, legend=True, color='b')
        elif kind=='kde': # kde = density
            dataset.plot(x=None, y=y, ax=ax, kind='kde', title='density', subplots=True, legend=True, color=None)
            #dataset.plot(x=None, y='length', ax=ax, kind='kde', title='title3', subplots=True, legend=True, color=None)
        elif kind=='area': # area
            df = dataset.copy()
            minim = df[[y]].min()
            df[[y]] = df[[y]] - minim
            #df[['length']]=df[['length']] - minim

            df.plot(x=None, y=y, ax=ax, kind='area', title='area', subplots=True, legend=True, color='b')
        else: # pie
            df = dataset.copy()
            minim = df[[y]].min()
            df[[y]] = df[[y]] - minim

            df.plot(x=None, y=y, ax=ax, kind='pie', title='pie chart', subplots=True, legend=True)



        #if style=='k.':
        #    dataset.plot(x=x, y=y, ax=ax, kind=kind, title=title, subplots=True, style=style, legend=False, c=color) # style='k.' kind='hist'
        #else:
        #    dataset.plot(x=x, y=y, ax=ax, kind=kind, title=title, subplots=True, style=style, legend=False, color=color)

        #ax.set_xlabel(xlabel)
        #ax.set_ylabel(ylabel)

    def plot_data(self, dataset, ax, x=None, y=None, kind='line', title=None, style=None, xlabel='', ylabel='', color='b'):

        
        if kind == 'line':# line
            dataset.plot(x=None, y=x, ax=ax, kind='line', subplots=True, style=None, legend=True, color='b')
            dataset.plot(x=None, y=y, ax=ax, kind='line', title='line', subplots=True, style=None, legend=True, color='r')
        elif kind=='box': # box
            dataset.boxplot(column=[x, y], ax= ax)
            #dataset.plot(x='length', y='width', ax=ax, kind='box', title='title1', grid=True, subplots=True, legend=True, color='b')
        elif kind=='kde': # kde = density
            dataset.plot(x=None, y=x, ax=ax, kind='kde', subplots=True, legend=True, color='b')
            dataset.plot(x=None, y=y, ax=ax, kind='kde', title='density', subplots=True, legend=True, color='r')
        else: # scatter
            dataset.plot(x=x, y=y, ax=ax, kind='scatter', title='scatter', subplots=True, legend=True, c=None)


        #if style=='k.':
        #    dataset.plot(x=x, y=y, ax=ax, kind=kind, title=title, subplots=True, style=style, legend=False, c=color) # style='k.' kind='hist'
        #else:
        #    dataset.plot(x=x, y=y, ax=ax, kind=kind, title=title, subplots=True, style=style, legend=False, color=color)

        #ax.set_xlabel(xlabel)
        #ax.set_ylabel(ylabel)

    def plot_multivariate_data(self, dataset, ax, columns, kind='line', title=None, style=None, xlabel='', ylabel='', color='b'):
        
        #self.comboBoxType.addItems(['line','bar', 'hist', 'box', 'kde', 'area', 'pie'])

        if kind == 'line':# line
            for column in columns:
                dataset.plot(x=None, y=column, ax=ax, kind='line', title='line', subplots=True, style=None, legend=True, color=None)
            #dataset.plot(x=None, y='length', ax=ax, kind='line', title='title1', subplots=True, style=None, legend=True, color='r')
        elif kind=='box': # box
            dataset.boxplot(column=columns, ax = ax)
            #dataset.plot(x='length', y='width', ax=ax, kind='box', title='title1', grid=True, subplots=True, legend=True, color='b')
        else: # kde = density
            for column in columns:
                dataset.plot(x=None, y=column, ax=ax, kind='kde', title='density', subplots=True, legend=True, color=None)
            #dataset.plot(x=None, y='length', ax=ax, kind='kde', title='title3', subplots=True, legend=True, color=None)

        
    def histogram(self, dataset, column=None, grid=True, bins=10):
        dataset.hist(column=column, grid=grid, bins=bins, color='b')
        plt.show()

    def resample_plot(self, dataset, frequency='A', functions=['mean']):
        upsampled = dataset.resample(frequency).apply(functions)
        upsampled.plot(subplots=True)

    def lag_plot(self, dataset, ylabel='', lags=None):
        if lags is None:
            lag_plot(dataset, c='b', edgecolor='black')
            plt.xlabel('lag 1')
            plt.ylabel(ylabel)
        else:
            values = pd.DataFrame(dataset.values)
            columns = [values]
            for i in range(1, (lags+1)):
                columns.append(values.shift(i))
            df = pd.concat(columns, axis=1)

            columns = ['t']
            for i in range(1, (lags+1)):
                columns.append('lag ' + str(i))

            df.columns = columns
            result = df.corr()

            plt.figure(1)
            for i in range(1, (lags+1)):
                ax = plt.subplot(330 + i)
                plt.scatter(x=df['t'].values, y=df['lag ' + str(i)].values, c='b', edgecolor='black')
                plt.xlabel('lag ' + str(i))
                props = dict(boxstyle='square', facecolor='white', alpha=0.5)
                text = 'r=' + str(round(result['t'][i], 3))
                ax.text(0.03, 0.95, text, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)

        plt.show()

    def autocorrelation_plot(self, dataset):
        autocorrelation_plot(dataset)
        plt.show()

    def plot_xy(self, dataset, x, y, xlabel, ylabel, linestyle='-', linewidth=1.0, color='blue'):
        plt.plot(dataset[x], dataset[y], linestyle=linestyle, linewidth=linewidth, color=color)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.show()
