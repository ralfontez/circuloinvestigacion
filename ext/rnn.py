# -*- coding: utf-8 -*-
import os
import keras.backend as K
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM, Flatten
from keras.optimizers import Adadelta, SGD, RMSprop, Adagrad, Adam, Adamax
import matplotlib.pyplot as plt
import numpy as np

np.random.seed(126)

class RNN:
    def __init__(self, n_inputs, n_outputs=1):
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs

    def __call__(self, hidden_layers=(8,), optimizer='adam', activation='relu', kernel_initializer ='random_normal'):
        n_inputs = self.n_inputs
        self.model = Sequential()
        self.model.add(LSTM(units=n_inputs*2, input_shape=(n_inputs, 1), return_sequences=False))
        #self.model.add(LSTM(n_inputs*2, return_sequences=True))

        #for i in range(hidden_layers[0]-2):
        #    self.model.add(LSTM(n_inputs*2, return_sequences=True))
        #self.model.add(LSTM(n_inputs*2))
        #self.model.add(Dense(n_inputs*2,activation='relu'))
        #self.model.add(Dense(n_inputs*2,activation='tanh'))
        self.model.add(Dense(self.n_outputs, activation='linear'))
        #self.model.add(Dense(self.n_outputs))
        self.model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['mse'])

        return self.model

    def train(self, x_train, y_train, epochs, batch_size=32, verbose=0):
        self.history = self.model.fit(
                                x_train,
                                y_train,
                                epochs=epochs,
                                batch_size=batch_size,
                                verbose=verbose
                                )
        return self.history

    def evaluate(self, x_test, y_test, verbose=1):
        score = self.model.evaluate(
                                    x_test,
                                    y_test,
                                    verbose=verbose
                                    )
        return score

    def predict(self, x_test):
        return self.model.predict(x_test)

    def plot_history(self, ax):
        ax.plot(self.history.history['loss'])
        ax.set_ylabel('loss')
        ax.set_xlabel('epoch')
        ax.legend(['train'], loc='upper right', fontsize=10)
        
