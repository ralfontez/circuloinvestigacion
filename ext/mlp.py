# -*- coding: utf-8 -*-
import os
import keras.backend as K
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adadelta, SGD, RMSprop, Adagrad, Adam, Adamax
import matplotlib.pyplot as plt
import numpy as np

np.random.seed(126)

class MLP:
    def __init__(self, n_inputs, n_outputs=1):
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs

    def __call__(self, hidden_layers=(8,), optimizer='adam', activation='relu', kernel_initializer ='random_normal'):
        n_inputs = self.n_inputs

        self.model = Sequential()

        for i, n_hidden in enumerate(hidden_layers):
            self.model.add(Dense(
                                units=n_hidden,
                                input_dim=n_inputs,
                                kernel_initializer=kernel_initializer,
                                activation=activation
                                ))
            n_inputs = n_hidden

        self.model.add(Dense(
                            units=self.n_outputs,
                            kernel_initializer=kernel_initializer,
                            activation='linear'
                            ))

        self.model.compile(
                          loss="mean_squared_error",
                          optimizer= optimizer,
                          metrics=['mse'])

        return self.model

    def train(self, x_train, y_train, epochs, batch_size=32, verbose=0):
        self.history = self.model.fit(
                                x_train,
                                y_train,
                                epochs=epochs,
                                batch_size=batch_size,
                                verbose=verbose
                                )
        return self.history

    def evaluate(self, x_test, y_test, verbose=1):
        score = self.model.evaluate(
                                    x_test,
                                    y_test,
                                    verbose=verbose
                                    )
        return score

    def predict(self, x_test):
        return self.model.predict(x_test)

    def plot_history(self, ax):
        ax.plot(self.history.history['loss'])
        ax.set_ylabel('loss')
        ax.set_xlabel('epoch')
        ax.legend(['train'], loc='upper right', fontsize=10)
        
